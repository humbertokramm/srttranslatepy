import argparse
import os
# Language Translator
from googletrans import Translator  # Import Translator module from googletrans package

def getListOfFiles(dirName):
	# create a list of files
	# names in the given directory 
	listOfFile = os.listdir(dirName)
	allFiles = list()
	# Iterate over all the entries
	for entry in listOfFile:
		# Create full path
		allFiles.append(entry)
	return allFiles         
	
def translateFilesInDirectory(dirName,language_ = 'pt'):
	# Get the list of all files in directory tree at given path
	listOfFiles = getListOfFiles(dirName)
	# Print the files
	for elem in listOfFiles:
		if(elem[-4:] == '.srt'): 
			print(elem)
			criaArquivoTraduzido(elem,language_)

def removeChar (lista,string):
	newstr = string
	for x in range(len(lista)):
		newstr = newstr.replace(lista[x],"")
	for x in range(len(lista)):
		newstr = newstr.replace(lista[x],"")
	newstr = newstr.replace('\n'," ")
	return newstr

def quebraLinha(cor,linha):
	linha = linha.replace('\n','')
	cor = cor.replace('\n','')
	quebra = linha.find(' ',30)
	strLinha = ''
	if(quebra > 0):
		while(quebra > 0):
			strLinha += cor
			strLinha += linha[:quebra]
			if(len(cor)>2): strLinha += '</font>\n'
			else: strLinha += '\n'
			linha = linha[quebra+1:]
			quebra = linha.find(' ',30)
	strLinha += cor
	strLinha += linha
	if(len(cor)>2): strLinha += '</font>\n\n'
	else: strLinha += '\n\n'
	return strLinha

def traduzArray(array, to='pt'):
	translator = Translator() # Create object of Translator.
	saida = ""
	nLinhas = len(array)
	divisoes = int(nLinhas/200)+1
	resto = nLinhas%200
	arrayBloco = []
	
	#Verifica se não houve erros
	if(nLinhas == 0): return ['error']
	for x in range(divisoes):
		arrayBloco.append(x*200)
	
	if(resto>0): arrayBloco.append(nLinhas)
	arrayBloco[-1] -= 1
	
	for bloco in range(len(arrayBloco)):
		start=arrayBloco[bloco]
		bloco += 1
		end = arrayBloco[bloco]+1
		#Bloco de repetição
		texto = ""
		for x in range(start,end):
			texto += array[x]
			texto += '\n'
		translated = translator.translate(texto, dest=to) 
		saida += translated.text
		
		if(len(arrayBloco) < bloco+2): break;
	
	saida = saida.split('\n')
	return saida


def criaArquivoTraduzido(file_name,language_):
	#Inserir aqui todos os simbolos que devem ser removidos durante a tradução
	stopWords = [
		'</font>',
		'<font color="#ffffff">', '<font color="#FFFFFF">',
		'<font color="#11ff11">', '<font color="#11FF11">',
		'<font color="#00ffff">', '<font color="#00FFFF">',
		'<font color="#ffff00">', '<font color="#FFFF00">',
		'<font color="white">', '<font color="green">',
		'{\\an1}', '{\\an2}', '{\\an3}', '{\\an4}',
		'{\\an5}', '{\\an6}', '{\\an7}', '{\\an8}', '{\\an9}'
	]

	index = []
	tempo = []
	cor = []
	fala = []
	traduzido =[]
	novalinha = 1
	f = open(file_name, encoding='utf-8')
	lines = f.readlines()

	for line in lines:
		if(line[:-1].isdigit()):
			index.append(line)
			novalinha = 0
		elif(line.find(' --> ')>0):
			tempo.append(line)
		elif(line.find('font color=')>0):
			marcaEnd = line.find('">')+2
			marcaStart = line.find('<font')
			if(novalinha < 1):
				cor.append(line[marcaStart:marcaEnd])
				fala.append(removeChar(stopWords,line))
			else:
				fala[-1] += removeChar(stopWords,line)
			novalinha += 1
		elif(not(line=='\n')):
			if(novalinha < 1):
				cor.append('')
				fala.append(line.replace('\n'," "))
			else:
				fala[-1] += line.replace('\n'," ")
			novalinha += 1

	#traduzido = fala
	traduzido = traduzArray(fala,to=language_)

	if not os.path.exists('output'): os.makedirs('output')
	my_file= open('output/'+file_name[:-4]+'-'+language_+file_name[-4:],"w+", encoding='utf-8')
	for x in range(len(traduzido)):
		my_file.write(index[x])
		my_file.write(tempo[x])
		my_file.write(quebraLinha(cor[x],traduzido[x]))
		
	my_file.write(str(int(index[x])+1)+'\n')
	my_file.write(tempo[-1])
	my_file.write('<font color="#ffffff">Traduzido com a API</font>\n')
	my_file.write('<font color="#ffffff">do google tradutor para python</font>\n')
	my_file.write('<font color="#ffffff">http://bit.ly/ProjTranslate </font>\n')
    

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-l", "--language", help = "idioma")
ap.add_argument("-f", "--file", 	help = "nome do arquivo")
args = vars(ap.parse_args())

dir_path = os.path.dirname(os.path.realpath(__file__))

#Idioma
language = str(args["language"])
if(language == 'None'):language = 'pt'

#Nome do Arquivo
filename = str(args["file"])
if(filename == 'None'):
	translateFilesInDirectory(dirName = dir_path, language_ = 'pt')
else: criaArquivoTraduzido(filename,language)




